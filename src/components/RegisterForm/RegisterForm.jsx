import { useState } from 'react';
import { register } from '../../api/auth';
import './RegisterForm.scss';

// 1. Crear un estado vacío con los campos que contendrá mi formulario
// 2. Crear el formulario en HTML
// 3. Crear funcion para el inputChange
// 4. Crear función para enviar el formulario.
// 5. Llamar a la función que hace el fetch a nuestro servidor

const INITIAL_STATE = {
    username: '',
    email: '',
    password: '',
};

const RegisterForm = (props) => {
    const [formFields, setFormFields] = useState(INITIAL_STATE);
    const [error, setError] = useState(null);

    const handleFormSubmit = async (ev) => {
        // 1. Recopilar los datos (formFields);
        // 2. Enviar petición al back de autenticación.
        ev.preventDefault();

        try {
            const user = await register(formFields);
            props.saveUser(user);
            setError(null);
            setFormFields(INITIAL_STATE);
        } catch (error) {
            setError(error.message);
        }

    };

    const handleInputChange = (ev) => {
        const { name, value } = ev.target;

        setFormFields({ ...formFields, [name]: value });

        console.log(formFields);
    };

    return (
        <div className="register-form">
            <h3>Registro</h3>
            <form onSubmit={handleFormSubmit}>
                <label htmlFor="username">
                    <p>Nombre de usuario</p>
                    <input
                        type="text"
                        name="username"
                        id="username"
                        placeholder="Nombre de usuario"
                        onChange={handleInputChange}
                        value={formFields.username}
                    />
                </label>

                <label htmlFor="email">
                    <p>Email</p>
                    <input
                        type="email"
                        name="email"
                        id="email"
                        placeholder="Email"
                        onChange={handleInputChange}
                        value={formFields.email}
                    />
                </label>

                <label htmlFor="password">
                    <p>Contraseña</p>
                    <input
                        type="text"
                        name="password"
                        id="password"
                        placeholder="Contraseña"
                        onChange={handleInputChange}
                        value={formFields.password}
                    />
                </label>

                <div className="register-form__button">
                    <button type="submit">Registrarme</button>
                </div>
            </form>
            {error && <div className="register-form__error">
                {error}
            </div>}
        </div>

    )
}

export default RegisterForm;
