import './Navbar.scss';

const Navbar = (props) => {
    return (
        <nav className="nav">
            <div>Upgrade Auth</div>
            {props.user && <div>
                <span className="nav__text">
                    Bienvenido de nuevo, {props.user.username}
                </span>
                <button>Logout</button>
            </div>}
        </nav>
    )
}

export default Navbar;
