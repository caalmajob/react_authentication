import { useState } from 'react';
import { RegisterForm, LoginForm, Navbar } from './components';
import './App.scss';

const App = () => {
  const [user, setUser] = useState(null);
  
  const saveUser = user => {
    console.log('GUARDAR USUARIO: ', user);
    setUser(user);
  };

  return (
    <div className="app">
      <Navbar user={user} />
      <RegisterForm saveUser={saveUser} />
      <LoginForm saveUser={saveUser} />
    </div>
  );
}

export default App;
