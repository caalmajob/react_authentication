// register = https://node-auth-upgrade.herokuapp.com/auth/register;

const registerUrl = "http://localhost:4000/auth/register";
const loginUrl = "http://localhost:4000/auth/login";

export const register = async (userData) => {
    const request = await fetch(registerUrl, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
        },
        credentials: 'include',
        body: JSON.stringify(userData)
    });

    const response = await request.json();

    if(!request.ok) {
        throw new Error(response.message);
    }

    return response;
};

export const login = async (userData) => {
    const request = await fetch(loginUrl, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
        },
        credentials: 'include',
        body: JSON.stringify(userData)
    });
    const response = await request.json();

    if(!request.ok) {
        throw new Error(response.message);
    }

    return response;
}
